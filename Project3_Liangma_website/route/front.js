/* 前台模块 */
const express = require('express');
const router = express.Router();
/* 引入数据库模块 */
const webSiteModule = require('../modules/website');

//服务模块数据库
const ServiceModel = require('../modules/service.js');

router.get('/', (req, res) => {
    res.render('front/index1');
});

// 联系模块
router.get('/contact', async(req, res) => {
    const info = await webSiteModule.findOne();
    if (info) {
        res.render('front/index6', { info });
    } else {
        res.render('front/index6', { info: {} });
    }
});

//服务模块
router.get('/service', async(req, res) => {

    const info = await ServiceModel.find();

    res.render('front/service', { info: info });
});

module.exports = router;