// JavaScript Document
$(function(){
		   
	//给全局绑定touchstart事件，为了更兼容active替代hover在移动端中的效果	   
	//document.body.addEventListener('touchstart',function(){}); 	
	
	//设置整体比例
	/*10px = 0.1rem*/
	var scale = $("body").width()/1920;
	$("html").css("font-size",100 * scale + 'px');
	$(window).resize(function(){
		var scale = $("body").width()/1920;
		$("html").css("font-size",100 * scale + 'px');					  
	});
	
    //顶部--当小于980px时变成手机端
    wwHF();
    $(window).resize(function(){wwHF();});
    function wwHF(){
        var $ww = $(window).width();
        $ww>980?$(".Header-wrapper").addClass("on"):$(".Header-wrapper").removeClass("on");
		$ww>980?$(".Footer-wrapper").addClass("on"):$(".Footer-wrapper").removeClass("on");
		$(".Hnav a i").each(function(){$(this).css({"width":$(this).parents("a").find("p").width(),"margin-left":-($(this).parents("a").find("p").width())/2});});
    }
	
	$(".Hmenu-btn").bind("click",function(){$(this).hasClass("cur")?$(this).removeClass("cur").siblings(".Hnav").slideUp(300):$(this).addClass("cur").siblings(".Hnav").slideDown(300);})
    
    $(".Fnav dt").bind("click",function(){$(this).hasClass("cur")?$(this).removeClass("cur").siblings("dd").slideUp(300):$(this).addClass("cur").siblings("dd").slideDown(300);});


    
    
	//滚动渐隐出现
	var scrollthis;
	$(window).scroll(function() {
		if ($(window).scrollTop() > 0 && !$(".float-right-box").hasClass('show')) {
			$(".float-right-box").addClass('show');
			$(".float-right-box").stop(true,true).addClass('on');
		}
	});
    
	$(".float-right-box").hover(function() {
		$(this).stop(true,true).removeClass('on');
	}, function() {
		var _this = $(this);
		if ($(window).scrollTop() > 0) {
			_this.stop(true,true).addClass('on');
		}else{
			$(".float-right-box").removeClass('show');
		}
	});


	
	/*浮动qq*/
	$('.float_qq2').hover(function(){
	
		$(this).animate({"left":"-70px"},350)
	
	},function(){
	
	$(this).animate({"left":"0"},350)
	
	})	
	
	$('.float_qq3').hover(function(){
	
		$(this).animate({"left":"-116px"},350)
	
	},function(){
	
		$(this).animate({"left":"0"},350)
	
	})	
	
	/*$('.float_qq4').hover(function(){
	
		$(this).find('.float_shwx').show();
	
	},function(){
	
		$(this).find('.float_shwx').hide();
	
	})*/		
	
	$('.foot_dshare3').hover(function(){
	
		$('.foot_shWx').show();
	
	},function(){
	
		$('.foot_shWx').hide();
	
	})	
	
	$('.foot_dshare2').hover(function(){
	
		$('.foot_shTelWx').show();
	
	},function(){
	
		$('.foot_shTelWx').hide();
	
	})	
	
	
    //锚点 -- 滑动
    $('a[href*=#],area[href*=#]').click(function(){
        if(location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname){
            var $target = $(this.hash);
            $target = $target.length && $target || $('[name=' + this.hash.slice(1) + ']');
            if($target.length){
                var targetOffset = $target.offset().top;
                $('html,body').animate({scrollTop: targetOffset},1000);
                return false;
            }
        }
    });	

})

/*--返回顶部动画--*/
//goTop(500);//500ms内滚回顶部
function goTop(times,fn){
	if (navigator.userAgent.indexOf('Firefox') >= 0){//firefox专用()
		document.documentElement.scrollTop=0;
	}
	if(!!!times){
		$(window).scrollTop(0);
		return;
	}
	var sh=$('body').scrollTop();//移动总距离
	var inter=13.333;//ms,每次移动间隔时间
	var forCount=Math.ceil(times/inter);//移动次数
	var stepL=Math.ceil(sh/forCount);//移动步长
	var timeId=null;
	function ani(){
		!!timeId&&clearTimeout(timeId);
		timeId=null;
		//console.log($('body').scrollTop());
		if($('body').scrollTop()<=0||forCount<=0){//移动端判断次数好些，因为移动端的scroll事件触发不频繁，有可能检测不到有<=0的情况
			$('body').scrollTop(0);
			if(jQuery.isFunction(fn))fn();
			return true;
		}
		forCount--;
		sh-=stepL;
		$('body').scrollTop(sh);
		timeId=setTimeout(function(){ani();},inter);
	}
	ani();
}

/*--判断是否为IE9及以下版本--*/
function IE(fn){
	if(navigator.userAgent.indexOf("MSIE")>0){     
		if( (navigator.userAgent.indexOf("MSIE 7.0")>0) || (navigator.userAgent.indexOf("MSIE 8.0")>0) ||(navigator.userAgent.indexOf("MSIE 9.0")>0 && !window.innerWidth) || (navigator.userAgent.indexOf("MSIE 9.0")>0)){ 
			fn(); 
			return true;
		}
	}
}

function isScrolledIntoView(elem) {
	var docViewTop = $(window).scrollTop();
	var docViewBottom = docViewTop + $(window).height();
	var elemTop = $(elem).offset().top;
	if (elemTop - $(elem).height()/3 < docViewTop) {
		return true;
	}
}

/* 滚动效果 */
/*var scrollArrty = ['.aa'];	
var scrI = 0;
var scrHeight = window.screen.availHeight;	
$(window).scroll(function(){
    scrollFn();
})
function scrollFn(){		
    if(scrI==scrollArrty.length){
        return false;
    }
    var boxPos = $(scrollArrty[scrI]).offset().top;	
    var winPos = $(window).scrollTop()+scrHeight-300;
    if(boxPos<winPos){
        $(scrollArrty[scrI]).addClass('active');
        scrI++;
        if(scrI<scrollArrty.length){
            scrollFn();
        }else{
            return false;
        }
    }else{
        return false;
    }
}
scrollFn();*/