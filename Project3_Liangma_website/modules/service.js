const mongoose = require('../db/MongoDB.js');

//这个代码对网站配置表的操作，在开发的时候，一般我们会操作很多的表，那么我们需要再建立一个目录，对所有的表的模型进行管理
const ServiceSchema = mongoose.Schema({
    title: {
        type: String,
        default: '',
    },
    banner: {
        type: String,
    },
    intro: {
        type: String,
        default: '',
    },
    designOne: {
        type: String,
        default: '',
    },
    designTwo: {
        type: String,
        default: '',
    },
    designThree: {
        type: String,
        default: '',
    },
    designFour: {
        type: String,
        default: '',
    },
    designFive: {
        type: String,
        default: '',
    },
    designSix: {
        type: String,
        default: '',
    },
    designSeven: {
        type: String,
        default: '',
    },
}, { timestamps: true });
//可以接受三个参数，如果传递了三个参数，代表实际的表名，不以第一个为准
const ServiceModel = mongoose.model('Serivice', ServiceSchema, 'service');

ServiceModel.fields = {
    title: '标题',
    banner: '背景图',
    intro: "简介",
    designOne: "设计一",
    designTwo: "设计二",
    designThree: "设计三",
    designFour: "设计四",
    designFive: "设计五",
    designSix: "设计六",
    designSeven: "设计7",
}

//等下需要操作表的地方，需要使用到模型，所以模型也要暴露
module.exports = ServiceModel;