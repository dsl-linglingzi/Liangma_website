/* 引入 express 模块 */
const express = require('express');
/* 创建网站服务 */
const app = express();
/* 设置端口号 */
const port = 8082;

/* 正文解析器  用于解析post请求的数据 */
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/* 设置静态文件 */
app.use(express.static('public'));

/* 引入模板引擎 */
const artTemplate = require('./template/artTemplate');

/* 引入前台路由 */
const frontRouter = require('./route/front');
/* 引入后台路由 */
const indexRouter = require('./route/index');
const websiteRouter = require('./route/website');
// 管理员登录
const adminRouter = require('./route/admin');
const clientRouter = require('./route/client.js');

//服务模块前台路由
const ServiceRoute = require('./route/service.js');
//定义服务模块后台相关外置路由信息
app.use('/admin', ServiceRoute);

/* 模板引擎 */
artTemplate(app);

/* 后台管理模块 */
app.use('/admin', indexRouter);
app.use('/admin/website', websiteRouter);
app.use('/admin', adminRouter)
app.use(clientRouter);

/* 前台模块 */
app.use(frontRouter);


app.listen(port, () => {
    console.log(`project listening at http://localhost:${port}`);
    console.log('本地服务已经开启在8082端口');
});