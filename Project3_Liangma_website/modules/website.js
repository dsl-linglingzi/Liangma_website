/*  网站基本信息：
  1.网站的名称   websiteName
  2.网站的关键字   keywords
  3.网站的描述信息  description
  4.网站的版权信息   copyright
  5.网站的备案号   ICBC
  6.公司地址   address
  7.公司电话   tel
  8.联系电话   phone
  9.公司邮箱   email
  10.微信个人二维码   personCode
  11.微信公众号二维码   publicCode
*/
const mongoose = require('../db/MongoDB');
/* 创建表结构 */
const webSiteSchema = mongoose.Schema({
    siteName: String,
    keywords: String,
    description: String,
    copyright: String,
    ICBC: String,
    address: String,
    tel: String,
    phone: String,
    email: String,
    personCode: String,
    publicCode: String,
});

/* 暂存数据 */
const webSiteModule = mongoose.model('webSite', webSiteSchema);

module.exports = webSiteModule;