/* 后台模块 */
const express = require('express');
const router = express.Router();

router.get('/index', (req, res) => {
    res.render('admin/index');
});
router.get('/welcome', (req, res) => {
    res.render('admin/welcome');
});

//添加成功页面提示
router.get('/success',(req,res)=>{
    res.render('admin/success')
})

module.exports = router;