const express = require('express');

const url = require('url');

const path = require('path');

const multer = require('multer');

const router = express.Router();

const serviceModel = require('../modules/service.js');

//图片上传
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, path.join(__dirname, '../public/uploads/'))
    },
    filename: function(req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
            /*path.extname( file.originalname ) 可以获取到文件的扩展名 1.jpg==> .jpg*/
        cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname))
    }
});

const upload = multer({ storage: storage });



//信息添加
router.get('/service/add', async(req, res) => {

    // const serviceInfo = await serviceModel.find();

    res.render('service-add', { title: 'banner信息添加', info: {} });
})

//信息入库
router.post('/service/store', upload.single('banner'), async(req, res) => {
    //收集数据，然后定义表的模型，入库
    let {
        title,
        intro,
        designOne,
        designTwo,
        designThree,
        designFour,
        designFive,
        designSix,
        designSeven,
    } = req.body;

    let banner = '';
    if (req.file) {
        banner = req.file.filename;
    }

    const newObj = new serviceModel({
        title,
        banner,
        intro,
        designOne,
        designTwo,
        designThree,
        designFour,
        designFive,
        designSix,
        designSeven,
    });

    let error = newObj.validateSync(); // 验证表单信息是否合法

    if (error) {
        const errorRs = [];
        for (let attr in error.errors) {
            errorRs.push(`${serviceModel.fields[attr]} 字段：${error.errors[attr]}`);
        }
        let url = '/admin/service/add';
        let time = 3;
        res.render('error', { errorRs, url, time });
        return;
    }

    //信息合法
    try {
        let info = await newObj.save();
        res.redirect('/admin/service/list');

    } catch (error) {
        res.redirect('/admin/service/add');
    }
})

//信息展示
router.get('/service/list', async(req, res) => {

    const infos = await serviceModel.find();

    res.render('service-list', { title: '信息展示', infos });
});

//删除  路由参数
router.get('/service/delete/:id', async(req, res) => {
    //删除的时候，需要传递一个分类的id
    //console.log(req.params);
    //res.json()可以把一个js对象转换为一个对象格式的字符串，方便查看  res.json会把格式化字符串的对象响应给浏览器
    //res.json(req.params);
    let proId = req.params.id;

    try {
        await serviceModel.deleteOne({ _id: proId });
        res.redirect('back');
    } catch (error) {
        res.redirect('back');
    }

})

//使用查询字符串传递(删除)
router.get('/service/del', async(req, res) => {
    //console.log(url.parse(req.url, true));
    //res.json(url.parse(req.url, true));
    let proId = url.parse(req.url, true).query.id;
    try {
        await serviceModel.deleteOne({ _id: proId });
        res.redirect('back');
    } catch (error) {
        res.redirect('back');
    }
})

//信息编辑
router.get('/service/edt/:id', async(req, res) => {
    let proId = req.params.id;
    //在编辑之前，应该先展示编辑内容的表单
    let info = await serviceModel.findOne({ _id: proId });
    const serviceInfo = await serviceModel.find();
    res.render('service-edt', { title: '产品资源编辑', info, serviceInfo });
});
router.post('/service/edtstore', upload.single('banner'), async(req, res) => {
    //收集数据，然后定义表的模型，入库
    let {
        _id,
        title,
        intro,
        designOne,
        designTwo,
        designThree,
        designFour,
        designFive,
        designSix,
        designSeven,
    } = req.body;

    let banner = '';
    //判断是否更新了图片
    if (req.file) {
        banner = req.file.filename;
    }

    //更具当前ID去查找记录然后更新
    serviceModel.findById(_id, async(error, doc) => {
        if (error) {
            res.redirect('back');
        } else {
            doc.title = title;
            doc.intro = intro;
            doc.designOne = designOne;
            doc.designTwo = designTwo;
            doc.designThree = designThree;
            doc.designFour = designFour;
            doc.designFive = designFive;
            doc.designSix = designSix;
            doc.designSeven = designSeven;
            if (banner) {
                doc.banner = banner;
            }

            await doc.save();
            res.redirect('/admin/service/list');
        }
    });

})

module.exports = router;