const express = require('express');
const router = express.Router();
const path = require('path');

/* 引入数据库模块 */
const webSiteModule = require('../modules/website');

/* 引入上传照片中间件 */
var multer = require('multer');
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, path.join(__dirname, '../public/uploads'));
    },
    filename: function(req, file, cb) {
        cb(null, file.fieldname + path.extname(file.originalname));
    }
})
var upload = multer({ storage: storage });

/* 网站基本信息添加和修改 */
router.post('/baseMsg', upload.fields([{ name: 'personCode' }, { name: 'publicCode' }]), (req, res) => {
    /* 照片上传功能模块 */
    // console.log('文件：', req.files);
    let attrCode = { personCode: '', publicCode: '' }
    if (req.files) {
        /* 有文件：代表更新操作，保存到数据库*/
        for (let value in req.files) {
            attrCode[value] = req.files[value][0].originalname;
        }
    }

    // console.log('提交的全部信息：', req.body);

    let { _id, siteName, keywords, description, copyright, ICBC, address, tel, phone, email } = req.body;

    /* 判断 id 是否有值 */
    if (_id) {
        /* 有值：执行添加操作 */
        webSiteModule.findById(_id, (error, data) => {
            if (error) {
                res.send('update failure！');
            } else {
                /* 更新数据 */
                data.siteName = siteName;
                data.keywords = keywords;
                data.description = description;
                data.copyright = copyright;
                data.ICBC = ICBC;
                data.address = address;
                data.tel = tel;
                data.phone = phone;
                data.email = email;
                data.personCode = attrCode.personCode;
                data.publicCode = attrCode.publicCode;
                /* 将新的数据保存到数据库 */
                data.save((error, data => {
                    if (error) {
                        res.send('upadate failure！');
                    } else {
                        // 重定向回原界面
                        res.redirect('/admin/website/add');
                    }
                }));
            }
        });
    } else {
        /* 无值：执行添加操作 */
        /* 创建表并将数据入库 */
        let webSiteObj = new webSiteModule({ siteName, keywords, description, copyright, ICBC, address, tel, phone, email, personCode: attrCode.personCode, publicCode: attrCode.publicCode });
        /* 保存到数据库 */
        webSiteObj.save((error, data) => {
            if (error) {
                res.send('add failure!')
            } else {
                /* 重定向回到原页面 */
                res.redirect('/admin/website/add');
            }
        });
    }
});

/* 后台信息管理 */
router.get('/add', async(req, res) => {
    // res.render('site-add');
    /* 查看数据库是否有数据 */
    const info = await webSiteModule.findOne();
    // console.log(info);
    if (info) {
        /* 有数据，展示在页面 */
        res.render('admin/site-add', { info });
    } else {
        /* 没有数据，为空值 */
        res.render('admin/site-add', { info: {} });
    }
});

module.exports = router;