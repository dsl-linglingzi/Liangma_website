// 导入mongoose模块
const mongoose = require('../db/MongoDB');

const adminSchema = new mongoose.Schema({
    adminname: {
        type: String,
        required: true,
        unique: true,
        minlength: 3,
        maxlength: 12
    },
    password: {
        type: String,
        required: true,
    }
})

const Admin = mongoose.model('Admin', adminSchema);

//创建一个管理员账户
// Admin.create({
//     adminname: 'admin',
//     password: '123456'
// });

module.exports = Admin;