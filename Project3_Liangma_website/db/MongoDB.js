const mongoose = require('mongoose');
const dbConfig = require('../config/dbConfig');

// 连接数据库
mongoose.connect(dbConfig.myConfig, { useNewUrlParser: true,useUnifiedTopology: true }).then(() => {
    console.log('数据库连接成功!!');
}).catch(() => {
    console.log('数据库连接失败!!');
});

module.exports = mongoose;