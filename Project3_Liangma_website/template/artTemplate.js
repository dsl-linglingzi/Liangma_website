/* 模板引擎模块 */
const express = require('express');
const app = express();
const path = require('path');

module.exports = (app) => {
    // view engine setup
    // 当渲染后缀为html的文件时  express使用什么模板框架 
    app.engine('html', require('express-art-template'));
    app.set('view options', {
        debug: process.env.NODE_ENV !== 'production'
    });
    // 告诉express框架模板所在的位置
    app.set('views', path.join(__dirname, '../views'));
    // 告诉express模板框架的后缀时什么
    app.set('view engine', 'html');
}