/*-------------------------------------------------------------------------
 * IPRESSUM - Custom jQuery Scripts
 * ------------------------------------------------------------------------

	1.	Plugins Init
	2.	Site Specific Functions
	3.	Shortcodes
	4.  Other Need Scripts (Plugins config, themes and etc)
	
-------------------------------------------------------------------------*/

(function($){
	$(document).ready(function(){
	
	/*-----------COUNTDOWN INIT -------------*/
	jQuery('.counter-item').appear(function() {
		jQuery('.cnt-num').countTo();
		jQuery(this).addClass('funcionando');
		console.log('funcionando');
	});
		
	/*-----------PARALLAX INIT-------------*/
	function initParallax() {
		$('#ads').parallax("100%", 0.3);
		$('#zhenzi').parallax("100%",0.3);
	}
	initParallax();
		
	// new mixitup plugs
	$('#casemixup').mixItUp();
		
	//Initiat WOW JS
	new WOW().init();
		
	// nav bar codes
	// Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });
	
	$(window).scroll(function() {
		if (($("#mainNav").length > 0)) { 
			//if(($(this).scrollTop() > 0) && ($(window).width() > 767)) {
			if(($(this).scrollTop() > 0)) {
				$("#mainNav").addClass("navbar-fixed-top animated fadeInDown");
				$(".head-top").css("display","none");
			} else {	
				$("#mainNav").removeClass("navbar-fixed-top animated fadeInDown");
				$(".head-top").css("display","block");
			}
		};
	});

	$(window).load(function() {
		if (($("#mainNav").length > 0)) { 
			if(($(this).scrollTop() > 0) && ($(window).width() > 767)) {
				$("#mainNav").addClass("navbar-fixed-top animated fadeInDown");
				$("#ttab").css("display","none");
			} else {
				//$("#mainNav").removeClass("affix");
				$("#ttab").css("display","block");
			}
		};
	});
		
	//Scroll totop
	$(window).scroll(function() {
		if($(this).scrollTop() != 0 && ($(window).width() > 767)) {
			$(".scrollToTop").fadeIn();	
		} else {
			$(".scrollToTop").fadeOut();
		}
	});
	
	$(".scrollToTop").click(function() {
		$("body,html").animate({scrollTop:0},800);
	});
		

	});
})(this.jQuery);