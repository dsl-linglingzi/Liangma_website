const mongoose = require('../db/MongoDB');

const ClientSchema = mongoose.Schema({
    name: String,
    tel: String,
    content: String,
});
const ClientModel = mongoose.model('Client', ClientSchema);

module.exports = ClientModel;